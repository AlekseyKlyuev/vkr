<?php

use Illuminate\Database\Seeder;

class PosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $owner = ['position_name' => 'Создатель'];
            $responsible = ['position_name' => 'Ответственный'];
            $observer = ['position_name' => 'Наблюдатель'];

            DB::table('position')->insert($owner);
            DB::table('position')->insert($responsible);
            DB::table('position')->insert($observer);

    }
}
