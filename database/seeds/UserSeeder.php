<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@marketica.pro',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ],
        ];

        User::insert($users);

        $names = ['Петр', 'Евграф', 'Светлана', 'Наталья'];


        foreach ($names as $i => $name) {
            $data = [
                'name' => $name,
                'email' => $i . 'admin-marketica@gmail.com',
                    'password' => Hash::make('123123'),
            ];
            DB::table('users')->insert($data);
        }


    }

}
