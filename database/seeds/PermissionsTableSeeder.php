<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
//            тут новые
            [
                'id'    => 17,
                'title' => 'task_create',
            ],
            [
                'id'    => 18,
                'title' => 'task_edit',
            ],
            [
                'id'    => 19,
                'title' => 'task_show',
            ],
            [
                'id'    => 20,
                'title' => 'task_delete',
            ],
            [
                'id'    => 21,
                'title' => 'task_access',
            ],
            [
                'id'    => 22,
                'title' => 'task_comment_access',
            ],
            [
                'id'    => 23,
                'title' => 'task_comment_create',
            ],
            [
                'id'    => 24,
                'title' => 'task_management_access',
            ],
            [
                'id'    => 25,
                'title' => 'task_restore',
            ],
            [
                'id'    => 26,
                'title' => 'task_force_delete',
            ],
            [
                'id'    => 27,
                'title' => 'task_archive_access',
            ],
            [
                'id'    => 28,
                'title' => 'profile_password_edit',
            ],
            [
                'id'    => 29,
                'title' => 'profile_password_edit',
            ],
//
            [
                'id'    => 30,
                'title' => 'project_create',
            ],
            [
                'id'    => 31,
                'title' => 'project_edit',
            ],
            [
                'id'    => 33,
                'title' => 'project_delete',
            ],
            [
                'id'    => 34,
                'title' => 'project_access',
            ],
            [
                'id'    => 35,
                'title' => 'project_management_access',
            ],
            [
                'id'    => 36,
                'title' => 'task_marketica',
            ],
            [
                'id'    => 37,
                'title' => 'access_personal_data',
            ],
        ];

        Permission::insert($permissions);
    }
}
