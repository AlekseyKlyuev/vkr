<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTelegramColumInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('telegram_chat_id')
                ->nullable()
                ->default(null);
            $table->string('telegram_nickname')
                ->nullable()
                ->default(null);
            $table->boolean('is_blocked_telegram_bot')
                ->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('telegram_chat_id');
            $table->dropColumn('telegram_nickname');
            $table->dropColumn('is_blocked_telegram_bot');
        });
    }
}
