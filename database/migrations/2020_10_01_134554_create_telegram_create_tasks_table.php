<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramCreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_create_tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('chat_id');
            $table->text('username');
            $table->smallInteger('project_id')
                ->nullable();
            $table->smallInteger('step')
                ->nullable();
            $table->text('appointment')
                ->nullable();
            $table->text('body')
                ->nullable();
            $table->text('price')
                ->nullable();
            $table->text('contragent')
                ->nullable();
            $table->date('deadline')
                ->nullable();
            $table->text('observers')
                ->nullable();
            $table->text('responsible')
                ->nullable();
            $table->text('owner')
                ->nullable();
            $table->string('original_file')
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_create_tasks');
    }
}
