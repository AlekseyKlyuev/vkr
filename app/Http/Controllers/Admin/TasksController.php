<?php

namespace App\Http\Controllers\Admin;

use App\Events\CreateTaskEvent;
use App\Events\UpdateTaskEvent;
use App\Http\Requests\MassDestroyTaskRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Project;
use App\Models\Task;
use App\Models\TaskMedia;
use App\Models\User;
use App\Services\TaskService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Gate;

class TasksController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        abort_if(Gate::denies('task_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tasks = Auth::user()->tasks()->with(['observers', 'owner', 'responsible'])->distinct()->get()->unique();

        return view('admin.tasks.index', compact('tasks'));
    }

    /**
     * Отображение заявок маркетики
     * */
    public function observeMarketica()
    {
        abort_if(Gate::denies('task_marketica'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tasks = Task::with(['observers', 'owner', 'responsible'])
            ->where('contragent', '=', 'Маркетика')
            ->orWhere('project', '=', 'Маркетика')
            ->distinct()
            ->get()
            ->unique();

        return view('admin.tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        abort_if(Gate::denies('task_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $task = new Task();
        $users = User::all();

        $projects = Project::all();
        return view('admin.tasks.create', compact('users', 'task', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTaskRequest $request
     * @return RedirectResponse
     */
    public function store(StoreTaskRequest $request)
    {
        if ($request->hasFile('file')) {
            $pathToFile = $request->file('file')->store('uploads/tasks', 'public');
            $originalName = $request->file('file')->getClientOriginalName();
        } else {
            $pathToFile = null;
            $originalName = null;
        }

        $task = TaskService::addTask($request, $pathToFile, $originalName);

        event(new CreateTaskEvent($task));

        $request->session()->flash('message', 'Задача успешно создана!');

        return redirect()
            ->route('admin.tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        abort_if(Gate::denies('task_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $task = Task::findOrFail($id);
        $taskMedia = TaskMedia::getMedia($id);

        return view('admin.tasks.show', compact('task', 'taskMedia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Task $task
     * @return Application|Factory|Response|View
     */
    public function edit(Task $task)
    {
        abort_if(Gate::denies('task_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id');
        $taskMedia = TaskMedia::getMedia($task->id);

        return view('admin.tasks.edit', compact('users', 'task', 'taskMedia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTaskRequest $request
     * @param Task $task
     * @return RedirectResponse
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        if (TasksController::isApprovedTask($task)) {
            event(new UpdateTaskEvent($task));
        }


        if ($request->hasFile('file')) {
            Storage::disk('public')->delete($task->file);
            $pathToFile = $request->file('file')->store('uploads/tasks', 'public');
            $originalName = $request->file('file')->getClientOriginalName();
        } else {
            $pathToFile = $task->file;
            $originalName = $task->original_file;
        }

        TaskService::updateTask($request, $task, $pathToFile, $originalName);


        session()->flash('message', 'Задача успешно обновлена!');

        return redirect()
            ->route('admin.tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Task $task)
    {
        abort_if(Gate::denies('task_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $task->delete();

        return redirect()->route('admin.tasks.index');
    }

    public function approved(Task $task)
    {
        TaskService::approvedTask($task);

        session()->flash('message', 'Заявка успешно согласована');
        return redirect()->route('admin.tasks.show', compact('task'));
    }

    public function massDestroy(MassDestroyTaskRequest $request)
    {
        Task::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }


    public static function isApprovedTask($task) : bool
    {

        $responsibleCount = $task->observers()->count();
        $approvedCount = $task->observers()->wherePivot('is_approved', '=', true)->count();

        return $approvedCount == $responsibleCount;
    }
}
