<?php

namespace App\Http\Controllers\Admin;

use App\Models\Task;
use App\Models\TaskMedia;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ArchiveTaskController extends BaseController
{
    public function index()
    {
        abort_if(Gate::denies('task_archive_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tasks = Auth::user()->tasks()->with(['observers', 'owner', 'responsible'])->onlyTrashed()->distinct()->get()->unique();

//        $tasks = Auth::user()->tasks()->with(['observers', 'owner', 'responsible'])->distinct()->get();

        return view('admin.tasks.archive.index', compact('tasks'));
    }

    public function restore($id)
    {
        abort_if(Gate::denies('task_restore'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $task = Task::withTrashed()->findOrFail($id);
        $task->restore();
        session()->flash('message', 'Задача успешно восстановлена!');

        return redirect()
            ->route('admin.tasks.index');
    }

    public function destroy($id)
    {
        abort_if(Gate::denies('task_force_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $task = Task::withTrashed()->findOrFail($id);
        Storage::disk('public')->delete($task->file);
        //TODO удалять доп. файлы
        $task->forceDelete();
        session()->flash('message', 'Задача успешно удалена!');

        return redirect()
            ->back();
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        abort_if(Gate::denies('task_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $task = Task::withTrashed()->findOrFail($id);
        $taskMedia = TaskMedia::getMedia($id);

        return view('admin.tasks.archive.show', compact('task', 'taskMedia'));
    }


    public function massDestroy()
    {
//        foreach ($request->input('ids') as $id) {
//            $task = Task::withTrashed()->where('id', '=', $id)-first();
//            dd($task);
////            Storage::disk('public')->delete($task->file);
////            $task->forceDeleted();
//        }
//        Task::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
