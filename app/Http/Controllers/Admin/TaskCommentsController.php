<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\TaskComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskCommentsController extends Controller
{

    public function store(Request $request, Task $task)
    {
        TaskComment::create([
            'body' => $request->body,
            'task_id' => $task->id,
            'user_id' => Auth::id(),
        ]);
        return redirect()->back()->with('message', 'Комментарий успешно добавлен!');
    }
}
