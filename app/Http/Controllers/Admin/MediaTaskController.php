<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\TaskMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MediaTaskController extends Controller
{

    public function store($id, Request $request)
    {

        $pathToFile = $request->file('file')->store('uploads/tasks', 'public');
        $originalName = $request->file('file')->getClientOriginalName();

        TaskMedia::create([
            'path' => $pathToFile,
            'task_id' => $id,
            'original_name' => $originalName
        ]);

    }

    public function destroy(Request $request)
    {
        $id = $request->post('id');
        $media = TaskMedia::findOrFail($id);

        Storage::disk('public')->delete($media->path);
        $media->forceDelete();

        return [
          'status' => true
        ];
    }

}
