<?php


namespace App\Http\Controllers\Telegram\Callback;


use App\Http\Controllers\Admin\TasksController;
use App\Http\Controllers\Telegram\traits\TelegramTaskHelper;
use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Keyboard\Keyboard;

class ViewTaskTelegramController
{

    use TelegramTaskHelper;


    public static function viewTask($chatId, $user, $taskId)
    {
        $task = Task::all()->where('id', '=', $taskId)->first();


        $filteredObs = $task->observers->where('id', $user->id);
        $filteredResp = $task->responsible->where('id', $user->id);
        $filteredOwner = $task->owner->where('id', $user->id);

        $isApproved = TasksController::isApprovedTask($task);

        $approvedText = $isApproved ? 'Согласована' : 'Требует согласования';

        $text = '
        CRM24.
        Статус: ' . $approvedText . '
        <b>Плательщик: ' . $task->project . '</b>
        <b>Контрагент: ' . $task->contragent . '</b>
        <b>Сумма: ' . $task->price . '</b>
        <b>Назначение: ' . $task->appointment . '</b>
        Постановщик: ' . $task->owner->first()->name . '
        <a href="' . config('app.url') . '/admin/tasks/' . $task->id . '">Посмотреть заявку</a>';

        if (count($filteredObs) > 0) {

            // Если есть соглосование не показывать кнопку согласовать, в противном случае показываем
            if ($filteredObs->first()->pivot->is_approved) {
                $reply_markup = null;
            } else {
                $reply_markup = Keyboard::make()
                    ->inline()
                    ->row(
                        Keyboard::inlineButton(['text' => 'Согласовать', 'callback_data' => '{"type":"approved_task","data":{"userId": ' . $user->id . ',"taskId":' . $task->id . '}}'])
                    );
            }

        }

        if (count($filteredResp) > 0 || count($filteredOwner) > 0) {
            $observerList = $task->observers;

            $i = 0;
            $text .= PHP_EOL . 'Наблюдатели:' . PHP_EOL;
            foreach ($observerList as $observer) {
                $i++;
                $isApprovedObserverText = $observer->pivot->is_approved ? 'Согласовано' : 'Ожидает';
                $text .= "{$i}) {$observer->name} - {$isApprovedObserverText}" . PHP_EOL;
            }

        }


        if (is_null($task->file)) {
            self::sendMessageWrapper($chatId, $text, $reply_markup ?? null, 'html');
        } else {
            self::sendDocumentWrapper($chatId, $text, $task, $reply_markup ?? null);
        }

    }


}
