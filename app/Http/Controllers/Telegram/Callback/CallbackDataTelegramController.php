<?php


namespace App\Http\Controllers\Telegram\Callback;

use App\Http\Controllers\Telegram\Message\CreateTaskTelegramController;
use App\Models\User;
use App\Services\TelegramService;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

class CallbackDataTelegramController
{
    public function handler(&$update)
    {
        $query = $update->getCallbackQuery();
        $data = json_decode($query->getData(), true);
        $chatId = $query->getFrom()->getId();

        $username = $query->getFrom()->username;
        $user = User::where('telegram_nickname', $username)->first();

        $createTaskInstance = CreateTaskTelegramController::getInstance();



        if ($data['type']) {
            switch ($data['type']) {
                case 'approved_task':
                    (new ApprovedTaskController())->approvedTask($data['data']['taskId'], $data['data']['userId'], $chatId);
                    break;
                case 'create_task':
                    $createTaskInstance->distributor($data['step'], $data, $chatId, $username);
                    break;
                case 'create_task_back':
                    $createTaskInstance->distributorBack($data['step'], $chatId, $username);
                    break;
                case 'view_task':
                    ViewTaskTelegramController::viewTask($chatId, $user, $data['task_id']);
                    break;
                default:
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => 'Прости, я тебя не понял, повтори действие еще раз👼',
                    ]);
                    break;
            }
        }
    }
}
