<?php


namespace App\Http\Controllers\Telegram\Callback;


use App\Models\Task;
use App\Services\TaskService;
use Telegram\Bot\Laravel\Facades\Telegram;

class ApprovedTaskController
{
    public function approvedTask($taskId, $userId, $chatId)
    {
        $task = Task::find($taskId);

        if ($task) {
            TaskService::approvedTask($task, $userId);
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'parse_mode' => 'markdown',
                'text' => "** {$task->title} **
Заявка была успешно согласована!",
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'parse_mode' => 'markdown',
                'text' => "**{$task->title}**, заявка не найдена!"
            ]);
        }
    }
}
