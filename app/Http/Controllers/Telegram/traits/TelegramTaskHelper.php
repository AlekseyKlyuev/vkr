<?php


namespace App\Http\Controllers\Telegram\traits;


use App\Http\Controllers\Telegram\Message\CreateTaskTelegramController;
use App\Http\Controllers\Telegram\Message\MessageTelegramController;
use App\Models\Position;
use App\Models\Project;
use App\Models\Task;
use App\Models\TelegramCreateTask;
use App\Models\User;
use cebe\markdown\GithubMarkdown;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait TelegramTaskHelper
{

    /**
     * @param $chatId
     * @param $step
     * @param null $field
     * @param null $data
     *
     * Функция обертка для обновления полей задачи, если поле не передаётся то просто обновить шаг
     *
     */
    public function updateTask($chatId, $step, $field = null, $data = null)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();
        if ($field !== null) {
            $createTask->update([
                'step' => $step,
                $field => $data
            ]);
        } else {
            $createTask->update([
                'step' => $step
            ]);
        }
    }

    /**
     * @param $chatId
     * @param $step
     * @param $text
     * @param false $withNextButton
     *
     * Функция обертка для вывода кнопок назад и далее, принимаем шаг на один меньше от основного шага, для перехода назад.
     * Соответственно прибавляем +2 для перехода на шаг вперед
     */
    public function sendBackDataWrapper($chatId, $step, $text, $withNextButton = false)
    {
        $nextStep = $step + 2;

        $keyboard = Keyboard::make()
            ->inline();

        if($withNextButton) {
            $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . $step . '"}']),
                Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . $nextStep . '"}']));
        } else {
            $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . $step . '"}']));
        }

        $this->sendMessageWrapper($chatId, $text, $keyboard);

    }


    /**
     * @param $step
     * @param $chatId
     * @param $username
     *
     * Инициализируем задачу при первом нажатии на кнопку "Создать заявку"
     */
    public static function initialCreateTask($step, $chatId, $username)
    {
        $reply_markup = Keyboard::make(['resize_keyboard' => true])
            ->row(Keyboard::inlineButton(['text' => MessageTelegramController::EXIT_CREATE_TASK_TEXT]));

        $owner = User::all()->where('telegram_nickname', '=', $username)->last();

        Telegram::sendMessage([
            'chat_id' => $chatId,
            'parse_mode' => 'markdown',
            'text' => 'Постановка задачи!',
            'reply_markup' => $reply_markup
        ]);

        TelegramCreateTask::create([
            'chat_id' => $chatId,
            'step' => $step,
            'username' => $username,
            'owner' => $owner->id
        ]);
    }


    /**
     * @param $chat_id
     * @param $text
     * @param null $reply_markup
     * @param string $parse_mode
     */
    public static function sendMessageWrapper($chat_id, $text, $reply_markup = null, $parse_mode = 'markdown')
    {
        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'parse_mode' => $parse_mode,
            'text' => $text,
            'reply_markup' => $reply_markup
        ]);
    }

    /**
     * @param $chat_id
     * @param $text
     * @param $task
     * @param null $reply_markup
     */
    public static function sendDocumentWrapper($chat_id, $text, $task, $reply_markup = null)
    {
        Telegram::sendDocument([
            'chat_id' => $chat_id,
            'document' => InputFile::create(Storage::disk('public')->path($task->file), $task->original_file),
            'parse_mode' => 'HTML',
            'caption' => $text,
            'reply_markup' => $reply_markup
        ]);
    }

    /**
     * @param $chatId
     * @param $document
     */
    public function uploadFile($chatId, $document)
    {
        $file_path = Telegram::getFile(['file_id' => $document->file_id]);
        $telegramLink = "https://api.telegram.org/file/bot" . env('TELEGRAM_BOT_TOKEN') . "/{$file_path->file_path}";

        $storage_path = storage_path();

        $timestamp = time();

        $path = $storage_path . "/app/public/uploads/telegram/{$timestamp}-{$document->file_name}";
        $pathToDb = "uploads/telegram/{$timestamp}-{$document->file_name}";
        $path_to_stream = fopen($path, 'w');
        $client = new Client();
        $response = $client->get($telegramLink, ['save_to' => $path_to_stream]);

        $this->updateTask($chatId, 8, 'original_file', $pathToDb);
    }


    /**
     * @param $keyboard
     * @param $data
     * @param $step
     * @param $userNames
     * @return mixed
     */
    public function getUserForInlineButtons($keyboard, $data, $step, $userNames)
    {

        foreach ($userNames as $i => $name) {
            $dataButton = '{"type":"create_task","step":"'. $step .'", "user_id":' . $i . '}';
            if (isset ($data['user_id']) && $i == $data['user_id']) {
                continue;
            }
            $keyboard->row(Keyboard::inlineButton([
                'text' => $name,
                'callback_data' => $dataButton
            ]));
        }

        return $keyboard;
    }

    /**
     * @param $createTask
     * @param $chatId
     * @param $username
     * @return bool
     *
     * Сразу отправить в конец если уже все поля заполнены
     */
    public function goToFinalCreateTask($createTask, $chatId, $username) : bool
    {
        $isFinal = $this->isFinalCreateTask($createTask);

        if($isFinal) {
            $this->distributor(CreateTaskTelegramController::FINAL_STEP, null, $chatId, $username);
            return true;
        }
        return false;
    }

    /**
     * @param $createTask
     * @return bool
     *
     * Меньше одного потому что файл может быть пустым
     */
    public function isFinalCreateTask($createTask) : bool
    {
        $attributes = $createTask->getAttributes();
        $count = collect($attributes)->whereNull()->count();
        return $count <= 1;
    }

    /**
     * @param $userNames
     * @param $observersList
     * @return string
     */
    public function getUserForText($userNames, $observersList)
    {
        $selectObservers = '';
        $observersListTmp = array_values(explode(',', $observersList));
        $counter = 0;
        foreach ($userNames as $i => $name) {
            if (in_array($i, $observersListTmp)) {
                $counter += 1;
                $selectObservers .= "{$counter}) " . $name . PHP_EOL;
            }
        }


        return $selectObservers;
    }


    /**
     * @param $userNamesList
     * @param $rolesList
     * @return array
     */
    public static function removeSelectedUsers($userNamesList, $rolesList) : array
    {
        $newUserNames = [];
        $rolesList = explode(',', $rolesList);
        foreach ($userNamesList as $i => $name) {
            if (!in_array($i, $rolesList)) {
                $newUserNames[$i] = $name;
            }
        }
        return $newUserNames;

    }


    /**
     * @param $setData
     */
    public function setDataWrapper($setData)
    {
        [
            'chatId' => $chatId,
            'field' => $field,
            'step' => $step,
            'sendBackText' => $sendBackText,
            'username' => $username,
            'data' => $data,
            'fieldText' => $fieldText,
            'fieldTextEnd' => $fieldTextEnd
        ] = $setData;

        $stepBack = $step - 1;
        $stepUp = $step + 1;

        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();

        if ($data !== '' && isset($data) || !is_null($createTask->$field)) {
            if (!is_null($data)) {
                $this->updateTask($chatId, $step, $field, $data);
                $this->goToFinalCreateTask($createTask, $chatId, $username) || $this->distributor($stepUp, null, $chatId, $username);
            } else {
                $this->sendBackDataWrapper($chatId, $stepBack, "{$fieldText} - {$createTask->$field} {$fieldTextEnd}", true);
            }

        } else {
            $this->sendBackDataWrapper($chatId, $stepBack, $sendBackText);
            $this->updateTask($chatId, $step);
        }

    }


}
