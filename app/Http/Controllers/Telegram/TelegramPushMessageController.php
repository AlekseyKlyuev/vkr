<?php


namespace App\Http\Controllers\Telegram;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\traits\TelegramTaskHelper;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramPushMessageController extends Controller
{
    use TelegramTaskHelper;

    public function pushMessageForAllUsers()
    {
        $users = User::all('telegram_chat_id')->toArray();

        //temp text, если будет необходимость в таком фунционале можно создать view и пушить сообщения из текстовой формы
        $text = "Коллеги!
Сервис CRM24  обновлен до Версии 2.0

В новой версии можно:
1. Поставить заявку на оплату из Telegram-бота.
2.  Посмотреть список заявок, которые требуют вашего внимания (не согласованы, не оплачены и так далее).

Для того, чтобы подключить обновления, нажмите /start

Хорошего дня!";

        foreach ($users as $user) {
            if (!is_null($user['telegram_chat_id']) && !empty($user['telegram_chat_id'])) {
                self::sendMessageWrapper($user['telegram_chat_id'], $text);
                Telegram::sendSticker([
                    'chat_id' => $user['telegram_chat_id'],
                    'sticker' => InputFile::create(asset('media/stickers/sticker-fred.webp'))
                ]);
            }
        }


    }


}
