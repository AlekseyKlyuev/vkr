<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\Callback\CallbackDataTelegramController;
use App\Http\Controllers\Telegram\Message\MessageTelegramController;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Telegram\Bot\Api;

class TelegramController extends Controller
{
    protected $telegram;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }

    public function setWebhook()
    {
        $url = config('app.url') . 'telegram/webhook'; // указываем url на который будет ссылаться бот
        $response = $this->telegram->setWebhook(['url' => $url]);
        dd($response);
    }


    public function webhookHandler()
    {
        try {
            $update = $this->telegram->commandsHandler(true);

            if ($update->isType('callback_query')) {
                (new CallbackDataTelegramController())->handler($update);
            } elseif ($update->isType('message')) {
                (new MessageTelegramController())->handler($update);
            }
        } catch (Exception $e) {
            Log::info($e->getMessage());
        } finally {
            return 'ok';
        }
    }
}
