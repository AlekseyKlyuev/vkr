<?php /** @noinspection ALL */


namespace App\Http\Controllers\Telegram\Message;


use App\Events\CreateTaskEvent;
use App\Http\Controllers\Telegram\traits\TelegramTaskHelper;
use App\Models\Project;
use App\Models\TelegramCreateTask;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class CreateTaskTelegramController
{

    use TelegramTaskHelper;

    private static $instances = [];
    const FINAL_STEP = 10;


    /**
     * @return CreateTaskTelegramController
     */
    public static function getInstance(): CreateTaskTelegramController
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }

    /**
     * @param $step
     * @param $data
     * @param $chatId
     * @param $username
     * @param null $document
     *
     * Главный распределитель
     *
     */
    public function distributor($step, $data, $chatId, $username, $document = null)
    {
        switch ($step) {
            case 1:
                $this->stepFirst($step, $chatId, $username, $data);
                break;
            case 2:
                $this->setContragent($chatId, $data, $username);
                break;
            case 3:
                $this->setPrice($chatId, $data, $username);
                break;
            case 4:
                $this->setAppointment($chatId, $data, $username);
                break;
            case 5:
                $this->setDate($chatId, $data, $username);
                break;
            case 6:
                $this->setObservers($chatId, $data);
                break;
            case 7:
                $this->setResponsible($chatId, $data);
                break;
            case 8:
                $this->setFile($chatId, $document);
                break;
            case 9:
                $this->setBody($chatId, $data, $username);
                break;
            case self::FINAL_STEP:
                $this->setFinalData($chatId);
                break;
            case 11:
                self::createdTask($chatId, $data);
                break;

        }
    }

    /**
     * @param $step
     * @param $chatId
     * @param $username
     *
     *  Распределитель для кнопки НАЗАД
     */
    public function distributorBack($step, $chatId, $username)
    {
        $this->updateTask($chatId, $step);
        $this->distributor($step, null, $chatId, $username);
    }


    /**
     * @param $step
     * @param $chatId
     * @param $username
     * @param $data
     *
     * Записываем заявки в бд и на этом шаге выбираем Проект/Плательщика заявки
     */
    public function stepFirst($step, $chatId, $username, $data)
    {
        $projects = Project::all();
        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();

        $keyboard = Keyboard::make()
            ->inline();
        foreach ($projects as $i => $project) {
            $dataButton = '{"type":"create_task","step":"1", "project_id":' . $project->id . '}';
            $keyboard->row(Keyboard::inlineButton([
                'text' => $project->name,
                'callback_data' => $dataButton
            ]));
        }


        if (isset($data['project_id']) && isset($data) || !empty($createTask)) {
            if (!is_null($data)) {
                $this->updateTask($chatId, 1, 'project_id', $data['project_id']);

                $this->goToFinalCreateTask($createTask, $chatId, $username) || $this->distributor(2, null, $chatId, $username);

            } else {
                $keyboard->row(Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . 2 . '"}']));
                $project = Project::find($createTask->project_id);
                self::sendMessageWrapper($chatId, "Плательщик - {$project->name} записан! Жмите далее! Или выберите заного", $keyboard);
            }

        } else {
            self::initialCreateTask($step, $chatId, $username);
            self::sendMessageWrapper($chatId, 'Выберите плательщика', $keyboard);
        }
    }

    /**
     * @param $chatId
     * @param $data
     * @param $username
     *
     *  На этом шаге предлагаем ввести Контрагента заявки
     */
    public function setContragent($chatId, $data, $username)
    {

        $setData = [
            'chatId' => $chatId,
            'field' => 'contragent',
            'step' => 2,
            'sendBackText' => 'Введите контрагента',
            'username' => $username,
            'data' => $data,
            'fieldText' => 'Контрагент',
            'fieldTextEnd' => 'записан, перезапишите его или жмите далее!'
        ];

        $this->setDataWrapper($setData);

    }

    /**
     * @param $chatId
     * @param $data
     *
     * На этом шаге предлагаем ввести Сумму заявки
     */
    public function setPrice($chatId, $data, $username)
    {

        $setData = [
            'chatId' => $chatId,
            'field' => 'price',
            'step' => 3,
            'sendBackText' => 'Введите сумму',
            'username' => $username,
            'data' => $data,
            'fieldText' => 'Сумма',
            'fieldTextEnd' => 'записана, перезапишите её или жмите далее!'
        ];

        $this->setDataWrapper($setData);

    }

    /**
     * @param $chatId
     * @param $data
     * @param $username
     *
     *   На этом шаге предлагаем ввести Назначение заявки
     */
    public function setAppointment($chatId, $data, $username)
    {

        $setData = [
            'chatId' => $chatId,
            'field' => 'appointment',
            'step' => 4,
            'sendBackText' => 'Введите Назначение',
            'username' => $username,
            'data' => $data,
            'fieldText' => 'Назначение',
            'fieldTextEnd' => 'записано, перезапишите его или жмите далее!'
        ];

        $this->setDataWrapper($setData);

    }

    /**
     * @param $chatId
     * @param $data
     * @param $username
     *
     * На этом шаге предлагаем ввести Дату окончания заявки
     */
    public function setDate($chatId, $data, $username)
    {

        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();
        if ($data !== '' && isset($data) || !is_null($createTask->deadline)) {
            if (!is_null($data)) {
                $timestamp = strtotime($data);
                $new_date = date("Y-m-d", $timestamp);

                if ($new_date === '1970-01-01') {
                    $this->sendBackDataWrapper($chatId, 4, 'Неверный формат даты, попробуйте ещё раз, нужный формат: дд-мм-гггг');
                } else {
                    $this->updateTask($chatId, 5, 'deadline', $new_date);
                    $this->goToFinalCreateTask($createTask, $chatId, $username) || $this->distributor(6, null, $chatId, $username);
                }


            } else {
                $this->sendBackDataWrapper($chatId, 4, "Дата - {$createTask->deadline} записана, перезапишите её в формате дд-мм-гггг или жмите далее!", true);
            }

        } else {
            $this->sendBackDataWrapper($chatId, 4, 'Введите дату закрытия заявки в формате дд-мм-гггг');
            $this->updateTask($chatId, 5);
        }

    }

    /**
     * @param $chatId
     * @param $data
     *
     * На это шаге предлагаем выбрать наблюдателей заявки
     */
    public function setObservers($chatId, $data)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();

        if (isset($data['clear_obs'])) {
            $createTask->observers = NULL;
            $createTask->save();
        }

        $observersList = $createTask->observers;
        $userNames = $tmpNames = User::all()->pluck('name', 'id')->toArray();

        if (isset($observersList) && !empty($observersList)) {
            $userNames = self::removeSelectedUsers($userNames, $observersList);
        }

        if (isset($data['user_id'])) {
            $createTask->observers .= $data['user_id'] . ',';
            $createTask->save();
        }

        $keyboard = Keyboard::make()
            ->inline();


        $this->getUserForInlineButtons($keyboard, $data, 6, $userNames);

        if (isset($observersList) && !empty($observersList) && !is_null($createTask->observers) || isset($data['user_id'])) {

            try {
                $selectObservers = $this->getUserForText($tmpNames, $createTask->observers);
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
            }

            if ($this->isFinalCreateTask($createTask)) {
                $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 5 . '"}']),
                    Keyboard::inlineButton(['text' => 'Сбросить список', 'callback_data' => '{"type":"create_task","step":"' . 6 . '","clear_obs":"yes"}']),
                    Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . self::FINAL_STEP . '"}']));
            } else {
                $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 5 . '"}']),
                    Keyboard::inlineButton(['text' => 'Сбросить список', 'callback_data' => '{"type":"create_task","step":"' . 6 . '","clear_obs":"yes"}']),
                    Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . 7 . '"}']));
            }


            self::sendMessageWrapper($chatId, 'Вы выбрали *наблюдателей*:' . PHP_EOL . $selectObservers, $keyboard);

        } else {
            $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 5 . '"}']));
            self::sendMessageWrapper($chatId, 'Выберите наблюдателей заявки', $keyboard);
        }


        $this->updateTask($chatId, 6);
    }

    /**
     * @param $chatId
     * @param $data
     *
     * На это шаге предлагаем выбрать ответственных заявки
     */
    public function setResponsible($chatId, $data)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();

        if (isset($data['clear_resp'])) {
            $createTask->responsible = NULL;
            $createTask->save();
        }

        $responsibleList = $createTask->responsible;
        $userNames = $tmpNames = User::all()->pluck('name', 'id')->toArray();

        if (isset($responsibleList) && !empty($responsibleList)) {
            $userNames = self::removeSelectedUsers($userNames, $responsibleList);
        }

        $keyboard = Keyboard::make()
            ->inline();

        $this->getUserForInlineButtons($keyboard, $data, 7, $userNames);


        if (isset($data['user_id'])) {
            $createTask->responsible .= $data['user_id'] . ',';
            $createTask->save();
        }


        if (isset($responsibleList) && !empty($responsibleList) && !is_null($createTask->responsible) || isset($data['user_id'])) {

            try {
                $selectResponsible = $this->getUserForText($tmpNames, $createTask->responsible);
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
            }

            if ($this->isFinalCreateTask($createTask)) {
                $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 6 . '"}']),
                    Keyboard::inlineButton(['text' => 'Сбросить список', 'callback_data' => '{"type":"create_task","step":"' . 7 . '","clear_resp":"yes"}']),
                    Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . self::FINAL_STEP . '"}']));
            } else {
                $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 6 . '"}']),
                    Keyboard::inlineButton(['text' => 'Сбросить список', 'callback_data' => '{"type":"create_task","step":"' . 7 . '","clear_resp":"yes"}']),
                    Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . 8 . '"}']));
            }


            self::sendMessageWrapper($chatId, 'Вы выбрали *ответственных* заявки:' . PHP_EOL . $selectResponsible, $keyboard);
        } else {
            $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 6 . '"}']));
            self::sendMessageWrapper($chatId, 'Выберите *ответственных* заявки', $keyboard);
        }


        $this->updateTask($chatId, 8);
    }

    /**
     * @param $chatId
     * @param $document
     *
     * Добавляем файл заявки
     */
    public function setFile($chatId, $document)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();
        $keyboard = Keyboard::make()
            ->inline();

        if ($this->isFinalCreateTask($createTask)) {
            $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 7 . '"}']),
                Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . self::FINAL_STEP . '"}']));
        } else {
            $keyboard->row(Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => '{"type":"create_task_back","step":"' . 7 . '"}']),
                Keyboard::inlineButton(['text' => 'Далее', 'callback_data' => '{"type":"create_task_back","step":"' . 9 . '"}']));
        }


        if (isset($document) || !is_null($createTask->original_file)) {

            if (!is_null($createTask->original_file) && !isset($document)) {
                self::sendMessageWrapper($chatId, 'Файл успешно загружен! Можете заменить его другим', $keyboard);

            }

            try {
                $this->uploadFile($chatId, $document);
                self::sendMessageWrapper($chatId, 'Файл успешно загружен!', $keyboard);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
            }

        } else {
            self::sendMessageWrapper($chatId, 'Добавьте файл или пропустите этот этап (чтобы файл загрузился его нужно загрузить как файл)', $keyboard);
        }


        $this->updateTask($chatId, 8);
    }

    /**
     * @param $chatId
     * @param $data
     * @param $username
     *
     * На это шаге предлагаем ввести описание заявки
     */
    public function setBody($chatId, $data, $username)
    {

        $setData = [
            'chatId' => $chatId,
            'field' => 'body',
            'step' => 9,
            'sendBackText' => 'Введите Описание',
            'username' => $username,
            'data' => $data,
            'fieldText' => 'Описание',
            'fieldTextEnd' => 'записано, перезапишите его или жмите далее!'
        ];

        $this->setDataWrapper($setData);

    }

    /**
     * @param $chatId
     *
     * Выводим информацию о заявке
     */
    public function setFinalData($chatId)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();
        $project = Project::find($createTask->project_id);
        $users = User::all('name', 'id')->toArray();


        $observerUsers = array_values(array_filter($users, fn($item) => in_array($item['id'], explode(',', $createTask->observers))));
        $responsibleUsers = array_values(array_filter($users, fn($item) => in_array($item['id'], explode(',', $createTask->responsible))));

        $finalText =
            '*Информация по задаче!*' . PHP_EOL
            . "Плательщик: {$project->name}" . PHP_EOL
            . "Сумма: {$createTask->price}" . PHP_EOL
            . "Контрагент: {$createTask->contragent}" . PHP_EOL
            . "Назначение: {$createTask->appointment}" . PHP_EOL
            . "Дата закрытия заявки: {$createTask->deadline}" . PHP_EOL
            . "Описание: {$createTask->body}" . PHP_EOL;

        $finalText .= 'Наблюдатели заявки:' . PHP_EOL;
        foreach ($observerUsers as $i => $observer) {
            $i += 1;
            $finalText .= "{$i}) {$observer['name']}" . PHP_EOL;
        }

        $finalText .= 'Ответственные заявки:' . PHP_EOL;
        foreach ($responsibleUsers as $i => $responsible) {
            $i += 1;
            $finalText .= "{$i}) {$responsible['name']}" . PHP_EOL;
        }

        $finalText .= "Вы можете редактировать или создать заявку!";

        $keyboard = Keyboard::make()
            ->inline();
        $keyboard->row(Keyboard::inlineButton(['text' => 'Описание', 'callback_data' => '{"type":"create_task_back","step":"' . 9 . '"}']),
            Keyboard::inlineButton(['text' => 'Плательщика', 'callback_data' => '{"type":"create_task_back","step":"' . 1 . '"}']),
            Keyboard::inlineButton(['text' => 'Контрагента', 'callback_data' => '{"type":"create_task_back","step":"' . 2 . '"}']));

        $keyboard->row(Keyboard::inlineButton(['text' => ' Наблюдателей', 'callback_data' => '{"type":"create_task_back","step":"' . 6 . '"}']),
            Keyboard::inlineButton(['text' => ' Сумму', 'callback_data' => '{"type":"create_task_back","step":"' . 3 . '"}']),
            Keyboard::inlineButton(['text' => ' Назначение', 'callback_data' => '{"type":"create_task_back","step":"' . 4 . '"}']));

        $keyboard->row(Keyboard::inlineButton(['text' => 'Дату закрытия', 'callback_data' => '{"type":"create_task_back","step":"' . 5 . '"}']),
            Keyboard::inlineButton(['text' => 'Ответственных', 'callback_data' => '{"type":"create_task","step":"' . 7 . '", "created_task":"yes"}']),
            Keyboard::inlineButton(['text' => 'Поменять файл', 'callback_data' => '{"type":"create_task","step":"' . 8 . '", "created_task":"yes"}']));

        $keyboard->row(Keyboard::inlineButton(['text' => 'Создать заявку!', 'callback_data' => '{"type":"create_task","step":"' . 11 . '", "created_task":"yes"}']));

        self::sendMessageWrapper($chatId, $finalText, $keyboard);
        $this->updateTask($chatId, self::FINAL_STEP);
    }


    /**
     * @param $chatId
     * @param $data
     *
     * Здесь создаём задачу и удаляем лог
     */
    public static function createdTask($chatId, $data)
    {
        if (isset($data['created_task'])) {
            $createTask = TelegramCreateTask::where('chat_id', $chatId)->first();
            $task = TelegramCreateTask::createdTaskWrapper($createTask);

            event(new CreateTaskEvent($task));

            if ($task) {
                MessageTelegramController::exitCreateTask($chatId);
                MessageTelegramController::defaultMessage($chatId, 'Заявка успешно создана!');
            }
        }
    }
}
