<?php


namespace App\Http\Controllers\Telegram\Message;


use App\Models\TelegramCreateTask;
use App\Models\User;
use App\Services\TelegramService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class MessageTelegramController
{

    const VIEW_TASK_TEXT = 'Посмотреть мои заявки';
    const CREATE_TASK_TEXT = 'Создать заявку';
    const EXIT_CREATE_TASK_TEXT = 'Выйти из создания заявки';

    public function handler(&$update)
    {
        $chat_id = $update->getMessage()->chat->id;
        $username = $update->getMessage()->from->username;
        $user = User::where('telegram_nickname', $username)->first();
        $document = $update->getMessage()->document;


        $createTaskInstance = CreateTaskTelegramController::getInstance();
        $message = $update->getMessage()->text;


        switch ($message) {
            case self::VIEW_TASK_TEXT:
                TelegramService::viewAllTasks($chat_id, $user);
                break;
            case self::CREATE_TASK_TEXT:
                $createTaskInstance->stepFirst(1, $chat_id, $username, $message);
                break;
            case '/start':
                $user->update([
                    'telegram_chat_id' => $chat_id,
                ]);
                self::defaultMessage($chat_id, 'Спасибо что авторизовался, жди уведомлений!');
                break;
            case self::EXIT_CREATE_TASK_TEXT:
                self::exitCreateTask($chat_id);
                self::defaultMessage($chat_id, 'Выход из создания заявки!');
                break;
            default:
                self::isCreateTask($chat_id, $message, $document);
                break;
        }
    }


    public static function defaultMessage($chat_id, $text)
    {
        $reply_markup = Keyboard::make(['resize_keyboard' => true])
            ->row(Keyboard::inlineButton(['text' => self::VIEW_TASK_TEXT]))
            ->row(Keyboard::inlineButton(['text' => self::CREATE_TASK_TEXT]));

        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'parse_mode' => 'markdown',
            'text' => $text,
            'reply_markup' => $reply_markup
        ]);
    }


    public static function isCreateTask($chat_id, $message, $document)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chat_id)
            ->where('step', '>=', 2)
            ->first();

        if ($createTask) {
            $createTaskInstance = CreateTaskTelegramController::getInstance();
            $createTaskInstance->distributor($createTask->step, $message, $chat_id, $createTask->username, $document);
        }

        return true;
    }

    public static function exitCreateTask($chat_id)
    {
        $createTask = TelegramCreateTask::where('chat_id', $chat_id)->first();
        $createTask->forceDelete();

    }
}
