<?php

namespace App\CommandBot;

use Telegram\Bot\Commands\Command;


/**
 * Class StartCommand
 */
class StartCommand extends Command
{
    /**
     * @var string Название команды
     */
    protected $name = 'start';

    /**
     * @var array Алиасы
     */
    protected $aliases = ['signup'];

    /**
     * @var string Описание команды
     */
    protected $description = 'Начать работу с ботом';

    public function handle()
    {
//        $update = $this->getUpdate();
//
//        $user_id = $update->getMessage()->from->id;
//
//        $this->telegram->sendMessage([
//            'chat_id' => $user_id,
//            'text' => 'Добро пожаловать! Ожидайте новых уведомлений'
//        ]);

        return 'Ok';

    }
}
