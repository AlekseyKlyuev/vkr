<?php


namespace App\Services;

use App\Events\ApprovedTaskEvent;
use App\Http\Controllers\Admin\TasksController;
use App\Models\Position;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use cebe\markdown\GithubMarkdown;
use Illuminate\Support\Facades\Log;

class TaskService
{
    public static function addTask($request, $pathToFile, $originalName)
    {
        return DB::transaction(function () use ($request, $pathToFile, $originalName) {

            $attribute['title'] = $request->input('project') . ': контрагент — ' . $request->input('contragent') . ', ' . $request->input('price') . '  ( ' . $request->input('appointment') . ')';
            $attribute['file'] = $pathToFile;
            $attribute['contragent'] = $request->input('contragent');
            $attribute['price'] = $request->input('price');
            $attribute['appointment'] = $request->input('appointment');
            $attribute['project'] = $request->input('project');
            $attribute['original_file'] = $originalName;
            $attribute['body'] = (new GithubMarkdown())->parse($request->input('body'));
            $attribute['raw_body'] = $request->input('body');
            $attribute['deadline'] = $request->input('deadline');
            $task = Task::create($attribute);

            DB::table('user_task')->insert([
                ['user_id' => Auth::id(), 'task_id' => $task->id, 'position_id' => Position::POSITION_OWNER, 'is_approved' => false]
            ]);

            foreach ($request->responsible as $responsible) {
                DB::table('user_task')->insert([
                    ['user_id' => $responsible, 'task_id' => $task->id, 'position_id' => Position::POSITION_RESPONSIBLE, 'is_approved' => false]
                ]);
            }

            foreach ($request->observers as $observer) {
                DB::table('user_task')->insert([
                    ['user_id' => $observer, 'task_id' => $task->id, 'position_id' => Position::POSITION_OBSERVER, 'is_approved' => false]
                ]);
            }
            return $task;
        });
    }

    public static function updateTask($request, Task $task, $pathToFile, $originalName)
    {

        try {

            $task->title = old('project', $task->project) . ': контрагент — ' . $request->input('contragent') . ', ' . $request->input('price') . '  ( ' . $request->input('appointment') . ')';
            $task->appointment = $request->appointment;
            $task->contragent = $request->contragent;
            $task->price = $request->price;

            if ($request->has('body')) {
                $task->body = (new GithubMarkdown())->parse($request->input('body'));
                $task->raw_body = $request->input('body');
            }
            $task->file = $pathToFile;
            $task->original_file = $originalName;
            $task->deadline = $request->deadline;
            $task->save();

            $sync = [];

            if ($request->has('observers')) {
                foreach ($request->observers as $observer) {
                    $sync['observers'][$observer] = [
                        'position_id' => Position::POSITION_OBSERVER,
                        'is_approved' => false,
                    ];
                }
            }

            if ($request->has('responsible')) {
                foreach ($request->responsible as $responsible) {
                    $sync['responsible'][$responsible] = [
                        'position_id' => Position::POSITION_RESPONSIBLE,
                        'is_approved' => false,
                    ];
                }
            }


            if ($request->has('owner')) {
                $sync['owner'][$request->owner] = [
                    'position_id' => Position::POSITION_OWNER,
                    'is_approved' => false,
                ];
            }


            $task->users()->sync([]);
            $task->users()->attach($sync['responsible']);
            $task->users()->attach($sync['observers']);
            $task->users()->attach($sync['owner']);


        } catch (\Throwable $e) {

            session()->flash('info', 'Ошибка при обновлении задачи');
        }
    }

    public static function approvedTask($task, $userId = null)
    {
        if (is_null($userId)) {
            $task->users()->sync([Auth::id() => ['is_approved' => true]], false);
        } else {
            $task->users()->sync([$userId => ['is_approved' => true]], false);
        }


        if (TasksController::isApprovedTask($task)) {
            event(new ApprovedTaskEvent($task));
        }
    }
}
