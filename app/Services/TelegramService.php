<?php


namespace App\Services;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramService
{

    const TASK_CHANGE_TEXT = 'Заявка отредактирована, требуется повторное согласование.';


    public static function formatPostText($text)
    {
        $formatText = strip_tags($text, '<b></b><strong></strong><i></i><em></em><a></a><code></code><pre></pre>');
        $formatText = str_replace('</p>', '\n', $formatText);
        $formatText = str_replace('<p>', '', $formatText);

        return $formatText;
    }

    public static function sendDocumentObserver($userId, $task_id, $owner, $chat_id, $title, $description, $file, $original_file)
    {
        $reply_markup = Keyboard::make()
            ->inline()
            ->row(
                Keyboard::inlineButton(['text' => 'Согласовать', 'callback_data' => '{"type":"approved_task","data":{"userId": ' . $userId . ',"taskId":' . $task_id . '}}'])
            );
        $response = Telegram::sendDocument([
            'chat_id' => $chat_id,
            'document' => InputFile::create(Storage::disk('public')->path($file), $original_file),
            'parse_mode' => 'HTML',
            'caption' => 'CRM24.
Требуется согласование заявки на оплату.
<b>' . $title . '</b>
' . $description . '
Постановщик: ' . $owner->first()->name . '

<a href="' . config('app.url') . '/admin/tasks/' . $task_id . '">Посмотреть заявку</a>',
            'reply_markup' => $reply_markup
        ]);

        if ( (int)$userId === 9 ) {
            Log::debug($response);
        }
    }

    public static function sendMessageObserver($userId, $task_id, $owner, $chat_id, $title, $description)
    {
        $reply_markup = Keyboard::make()
            ->inline()
            ->row(
                Keyboard::inlineButton(['text' => 'Согласовать', 'callback_data' => '{"type":"approved_task","data":{"userId": ' . $userId . ',"taskId":' . $task_id . '}}'])
            );
        $response = Telegram::sendMessage([
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
            'text' => 'CRM24.
Требуется согласование заявки на оплату.
<b>' . $title . '</b>
' . $description . '
Постановщик: ' . $owner->first()->name . '

<a href="' . config('app.url') . '/admin/tasks/' . $task_id . '">Посмотреть заявку</a>',
            'reply_markup' => $reply_markup
        ]);

        if ( (int)$userId === 9 ) {
            Log::debug($response);
        }
    }

    public static function reSendMessageObserver($userId, $task_id, $owner, $chat_id, $title, $description)
    {
        $reply_markup = Keyboard::make()
            ->inline()
            ->row(
                Keyboard::inlineButton(['text' => 'Согласовать', 'callback_data' => '{"type":"approved_task","data":{"userId": ' . $userId . ',"taskId":' . $task_id . '}}'])
            );

        $response = Telegram::sendMessage([
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
            'text' => 'CRM24.
Заявка отредактирована, требуется повторное согласование.
<b>' . $title . '</b>
' . $description . '
Постановщик: ' . $owner->first()->name . '

<a href="' . config('app.url') . '/admin/tasks/' . $task_id . '">Посмотреть заявку</a>',
            'reply_markup' => $reply_markup
        ]);

        if ( (int)$userId === 9 ) {
            Log::debug($response);
        }
    }

    public static function reSendDocumentObserver($userId, $task_id, $owner, $chat_id, $title, $description, $file, $original_file)
    {
        $reply_markup = Keyboard::make()
            ->inline()
            ->row(
                Keyboard::inlineButton(['text' => 'Согласовать', 'callback_data' => '{"type":"approved_task","data":{"userId": ' . $userId . ',"taskId":' . $task_id . '}}'])
            );

        $data = [
            'chat_id' => $chat_id,
            'document' => InputFile::create(Storage::disk('public')->path($file), $original_file),
            'parse_mode' => 'HTML',
            'caption' => 'CRM24.
Заявка отредактирована, требуется повторное согласование.
<b>' . $title . '</b>
' . $description . '
Постановщик: ' . $owner->first()->name . '

<a href="' . config('app.url') . '/admin/tasks/' . $task_id . '">Посмотреть заявку</a>',
            'reply_markup' => $reply_markup
        ];

        $response = Telegram::sendDocument($data);

        if ( (int)$userId === 9 ) {
            Log::debug($response);
        }
    }


    public static function viewAllTasks($chat_id, $user)
    {
        $tasks = $user->tasks;


        if (count($tasks) > 0) {
            $tasks = $tasks->unique();
            $keyboard = Keyboard::make()->inline();
            foreach ($tasks as $i => $task) {
                $dataButton = '{"type":"view_task", "task_id":"' . $task->id . '"}';
                $keyboard->row(Keyboard::inlineButton([
                    'text' => "{$task->project} : {$task->contragent} : {$task->price}",
                    'callback_data' => $dataButton
                ]));
            }

            $data = [
                'chat_id' => $chat_id,
                'text' => 'Выбери заявку',
                'reply_markup' => $keyboard,
            ];

        } else {
            $data = [
                'chat_id' => $chat_id,
                'text' => 'У вас нет активных заявок!'
            ];

        }


        Telegram::sendMessage($data);
    }


}
