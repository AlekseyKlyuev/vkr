<?php

namespace App\Listeners;

use App\Events\UpdateTaskEvent;
use App\Services\TelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Exceptions\TelegramSDKException;

class UpdateTaskListener
{
    /**
     * @param  UpdateTaskEvent  $event
     * @return void
     */
    public function handle(UpdateTaskEvent $event)
    {
        $descriptionTask = TelegramService::formatPostText($event->task->body);

        foreach ($event->task->observers as $observer) {
            if ($observer->telegram_chat_id) {
                try {
                    if ($event->task->file != null) {
                        TelegramService::reSendDocumentObserver($observer->id, $event->task->id, $event->task->owner, $observer->telegram_chat_id, $event->task->title, $descriptionTask, $event->task->file, $event->task->original_file);
                    } else {
                        TelegramService::reSendMessageObserver($observer->id, $event->task->id, $event->task->owner, $observer->telegram_chat_id, $event->task->title, $descriptionTask);
                    }
                } catch (TelegramSDKException $exception) {
                    session()->flash('info', 'Не всем наблюдателям удалось отправить уведомление, проверьте это!');
                }

            } else {
                session()->flash('info', 'Не всем наблюдателям удалось отправить уведомление, проверьте это!');
            }
        }
    }
}
