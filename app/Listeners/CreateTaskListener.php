<?php

namespace App\Listeners;

use App\Events\CreateTaskEvent;
use App\Services\TelegramService;
use Telegram\Bot\Exceptions\TelegramSDKException;

class CreateTaskListener
{
    /**
     * Handle the event.
     *
     * @param CreateTaskEvent $event
     * @return void
     */
    public function handle(CreateTaskEvent $event)
    {
        $descriptionTask = TelegramService::formatPostText($event->task->body);

        foreach ($event->task->observers as $observer) {
            if ($observer->telegram_chat_id) {
                try {
                    if ($event->task->file != null) {
                        TelegramService::sendDocumentObserver($observer->id, $event->task->id, $event->task->owner, $observer->telegram_chat_id, $event->task->title, $descriptionTask, $event->task->file, $event->task->original_file);
                    } else {
                        TelegramService::sendMessageObserver($observer->id, $event->task->id, $event->task->owner, $observer->telegram_chat_id, $event->task->title, $descriptionTask);
                    }
                } catch (TelegramSDKException $exception) {
                    session()->flash('info', 'Не всем наблюдателям удалось отправить уведомление, проверьте это!');
                }

            } else {
                session()->flash('info', 'Не всем наблюдателям удалось отправить уведомление, проверьте это!');
            }
        }
    }
}
