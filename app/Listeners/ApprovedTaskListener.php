<?php

namespace App\Listeners;

use App\Events\ApprovedTaskEvent;
use App\Services\TelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Laravel\Facades\Telegram;

class ApprovedTaskListener
{
    /**
     * Handle the event.
     *
     * @param ApprovedTaskEvent $event
     * @return void
     */
    public function handle(ApprovedTaskEvent $event)
    {
        $description = TelegramService::formatPostText($event->task->body);

        foreach ($event->task->responsible as $responsible) {
            if ($responsible->telegram_chat_id) {
                try {
                    Telegram::sendMessage([
                        'chat_id' => $responsible->telegram_chat_id,
                        'parse_mode' => 'HTML',
                        'text' => 'CRM24.
Требуется оплата заявки.
<b>'. $event->task->title .'</b>
' . $description . '
<a href="'. config('app.url') .'/admin/tasks/'. $event->task->id .'">Посмотреть заявку</a>',
                    ]);
                } catch (TelegramSDKException $exception) {
                    session()->flash('info', 'Уведомление о исполнении не удалось отправть всем ответственным!');
                }
            } else {
                session()->flash('info', 'Уведомление о исполнении не удалось отправть всем ответственным!');
            }
        }
    }
}
