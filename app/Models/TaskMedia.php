<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\TaskMedia
 *
 * @property int $id
 * @property string $path
 * @property int $task_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $original_name
 * @property-read \App\Models\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskMedia whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskMedia extends Model
{
    protected $guarded = [];


    public function task()
    {
        return $this->belongsTo(Task::class);
    }


    public static function getMedia($id)
    {
        return DB::table('task_media')
            ->where('task_id', $id)
            ->get();
    }

}
