<?php

namespace App\Models;

use cebe\markdown\GithubMarkdown;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\TelegramCreateTask
 *
 * @property int $id
 * @property int $chat_id
 * @property string $username
 * @property int|null $project_id
 * @property int|null $step
 * @property string|null $appointment
 * @property string|null $body
 * @property string|null $price
 * @property string|null $contragent
 * @property string|null $deadline
 * @property string|null $observers
 * @property string|null $responsible
 * @property string|null $owner
 * @property string|null $original_file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask query()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereAppointment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereContragent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereObservers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereOriginalFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereResponsible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramCreateTask whereUsername($value)
 * @mixin \Eloquent
 */
class TelegramCreateTask extends Model
{
    protected $guarded = [];


    public static function createdTaskWrapper($taskRequest)
    {
        $project = Project::all()->where('id', '=', $taskRequest->project_id)->last();
        $responsibles = array_filter(array_map(function ($item) {
            if ((integer)$item !== 0) {
                return (integer) $item;
            }
        }, explode(',', $taskRequest->responsible)));

        $observers = array_filter(array_map(function ($item) {
            if ((integer)$item !== 0) {
                return (integer) $item;
            }
        }, explode(',', $taskRequest->observers)));


        try {
            return DB::transaction(function () use ($taskRequest, $project, $responsibles, $observers) {

                $attribute['title'] = "{$project->name} : {$taskRequest->contragent} : {$taskRequest->price}";
                $attribute['file'] = $taskRequest->original_file;
                $attribute['contragent'] = $taskRequest->contragent;
                $attribute['price'] = $taskRequest->price;
                $attribute['appointment'] = $taskRequest->appointment;
                $attribute['project'] = $project->name;
                $attribute['original_file'] = $taskRequest->original_file;
                $attribute['body'] = (new GithubMarkdown())->parse("{$taskRequest->body}");
                $attribute['raw_body'] = $taskRequest->appointment;
                $attribute['deadline'] = $taskRequest->deadline;
                $task = Task::create($attribute);


                DB::table('user_task')->insert([
                    ['user_id' => $taskRequest->owner, 'task_id' => $task->id, 'position_id' => Position::POSITION_OWNER, 'is_approved' => false]
                ]);

                foreach ($responsibles as $responsible) {

                    DB::table('user_task')->insert([
                        ['user_id' => $responsible, 'task_id' => $task->id, 'position_id' => Position::POSITION_RESPONSIBLE, 'is_approved' => false]
                    ]);
                }

                foreach ($observers as $observer) {
                    DB::table('user_task')->insert([
                        ['user_id' => $observer, 'task_id' => $task->id, 'position_id' => Position::POSITION_OBSERVER, 'is_approved' => false]
                    ]);
                }
                return $task;
            });
        } catch (\Throwable $e) {

            Log::warning($e);
        }

    }


}
