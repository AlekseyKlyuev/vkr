<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\Task
 *
 * @property int $id
 * @property string $title
 * @property string $raw_body
 * @property string $body
 * @property string|null $deadline
 * @property int|null $project_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $file
 * @property string|null $original_file
 * @property string $contragent
 * @property \App\Models\Project|null $project
 * @property string $price
 * @property string $appointment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskComment[] $comments
 * @property-read int|null $comments_count
 * @property-read mixed $is_approved
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $observers
 * @property-read int|null $observers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $owner
 * @property-read int|null $owner_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $responsible
 * @property-read int|null $responsible_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task newQuery()
 * @method static \Illuminate\Database\Query\Builder|Task onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereAppointment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereContragent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereOriginalFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereProject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereRawBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Task withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Task withoutTrashed()
 * @mixin \Eloquent
 */
class Task extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function getIsApprovedAttribute()
    {
        return $this->observers()->wherePivot('user_id', '=', Auth::id())->wherePivot('task_id', '=', $this->id)->first()->pivot->is_approved;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_task')
            ->withPivot('id', 'position_id', 'is_approved');
    }

    public function owner()
    {
        return $this->belongsToMany(User::class, 'user_task')
            ->withPivot('id', 'position_id', 'is_approved')
            ->wherePivot('position_id','=', Position::POSITION_OWNER);
    }

    public function responsible()
    {
        return $this->belongsToMany(User::class, 'user_task')
            ->withPivot('id', 'position_id', 'is_approved')
            ->wherePivot('position_id','=',Position::POSITION_RESPONSIBLE);

    }

    public function observers()
    {
        return $this->belongsToMany(User::class, 'user_task')
            ->withPivot('id', 'position_id', 'is_approved')
            ->wherePivot('position_id','=', Position::POSITION_OBSERVER);
    }

    public function comments()
    {
        return $this->hasMany(TaskComment::class);
    }

    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }
}
