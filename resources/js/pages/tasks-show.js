document.addEventListener('DOMContentLoaded', () => {
    let linkFromTaskBody = document.querySelectorAll('.card__body p a')
    let linkFromTaskBodyNode = [...linkFromTaskBody];
    linkFromTaskBodyNode.forEach(item => item.target = "_blank")
})


document.addEventListener('click', event => {

    let mediaDeleteButton = event.target.closest('.card__file--delete')

    if (!mediaDeleteButton) return;

    let question = confirm('Вы уверены?')

    if (question) {
        let mediaId = mediaDeleteButton.dataset['id']

        let formData = new FormData
        formData.append('id', mediaId)

        fetch('/media-task/destroy', {
            method: 'POST',
            body: formData
        })
            .then(r => r.json())
            .then(() => window.location.reload())


    }


})
