document.addEventListener('click', event => {
    let saveTaskButton = event.target.closest('#save-task')

    if (!saveTaskButton) return;
    let observers = document.getElementById('observers')
    let responsible = document.getElementById('responsible')

    if (observers.value === '') {
        alert('Пустое поле наблюдатели')
        event.preventDefault()
    }

    if (responsible.value === '') {
        alert('Пустое поле ответственные')
        event.preventDefault()
    }

    saveTaskButton.submit()
})
