@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            Создание заявки
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route("admin.tasks.store") }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="form-group col-md-4">
                        <label class="required" for="project">Плательщик</label>
                        <select class="form-control" name="project" id="project">

                            @foreach($projects as $project)
                                <option value="{{ $project->name }}">{{ $project->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label class="required" for="contragent">Контрагент</label>
                        <input class="form-control {{ $errors->has('contragent') ? 'is-invalid' : '' }}" type="text" name="contragent" id="contragent" value="{{ old('contragent', '') }}">
                        @if($errors->has('contragent'))
                            <div class="invalid-feedback">
                                {{ $errors->first('contragent') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-4">
                        <label class="required" for="price">Сумма</label>
                        <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="text" name="price" id="price" value="{{ old('price', '') }}">
                        @if($errors->has('price'))
                            <div class="invalid-feedback">
                                {{ $errors->first('price') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="required" for="appointment">Назначение</label>
                        <input class="form-control {{ $errors->has('appointment') ? 'is-invalid' : '' }}" type="text" name="appointment" id="appointment" value="{{ old('appointment', '') }}">
                        @if($errors->has('appointment'))
                            <div class="invalid-feedback">
                                {{ $errors->first('appointment') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-6">
                        <label class="required" for="deadline">Выберите дату закрытия заявки</label>
                        <input class="form-control {{ $errors->has('deadline') ? 'is-invalid' : '' }}" type="date" name="deadline" id="deadline" required>
                        @if($errors->has('deadline'))
                            <div class="invalid-feedback">
                                {{ $errors->first('deadline') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="required" for="body">Введите описание заявки</label>
                    <textarea class="form-control body {{ $errors->has('body') ? 'is-invalid' : '' }}" type="body" name="body" id="body" required>{{ old('body') }}</textarea>
                @if($errors->has('body'))
                    <div class="invalid-feedback">
                        {{ $errors->first('body') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="required" for="observers">Выберите наблюдателей заявки</label>
                        <div style="padding-bottom: 4px">
                            <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                            <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                        </div>
                        <select class="form-control select2 {{ $errors->has('observers') ? 'is-invalid' : '' }}" name="observers[]" id="observers" multiple required>
                            @foreach($users as $user)
                                @if($user->id == Auth::id())
                                    @continue
                                @endif
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('user', [])) ? 'selected' : '' }}>{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('observers'))
                            <div class="invalid-feedback">
                                {{ $errors->first('observers') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label class="required" for="responsible">Выберите ответственных заявки</label>
                        <div style="padding-bottom: 4px">
                            <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                            <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                        </div>
                        <select class="form-control select2 {{ $errors->has('responsible') ? 'is-invalid' : '' }}" name="responsible[]" id="responsible" multiple required>
                            @foreach($users as $user)
                                @if($user->id == Auth::id())
                                    @continue
                                @endif
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('user', [])) ? 'selected' : '' }}>{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('responsible'))
                            <div class="invalid-feedback">
                                {{ $errors->first('responsible') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="required" for="title">Выберите файл</label>
                    <input class="form-control {{ $errors->has('file') ? 'is-invalid' : '' }}" type="file" name="file" id="file" value="{{ old('file', '') }}">
                    @if($errors->has('file'))
                        <div class="invalid-feedback">
                            {{ $errors->first('file') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>



@endsection
