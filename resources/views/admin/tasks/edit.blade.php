@extends('layouts.admin')

@section('content')
    <input hidden id="task-id" type="text" value="{{$task->id}}"/>

    <div class="card">
        <div class="card-header">
            Редактирование заявки
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route("admin.tasks.update", $task->id) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="price">Сумма</label>
                        <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="text"
                               name="price" id="price" value="{{ old('price', $task->price) }}">
                        @if($errors->has('price'))
                            <div class="invalid-feedback">
                                {{ $errors->first('price') }}
                            </div>
                        @endif
                    </div>


                    <div class="form-group col-md-3">
                        <label for="contragent">Контрагент</label>
                        <input class="form-control {{ $errors->has('contragent') ? 'is-invalid' : '' }}" type="text"
                               name="contragent" id="contragent" value="{{ old('contragent', $task->contragent) }}">
                        @if($errors->has('contragent'))
                            <div class="invalid-feedback">
                                {{ $errors->first('contragent') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-3">
                        <label for="appointment">Назначение</label>
                        <input class="form-control {{ $errors->has('appointment') ? 'is-invalid' : '' }}" type="text"
                               name="appointment" id="appointment" value="{{ old('appointment', $task->appointment) }}">
                        @if($errors->has('appointment'))
                            <div class="invalid-feedback">
                                {{ $errors->first('appointment') }}
                            </div>
                        @endif
                    </div>


                    <div class="form-group col-md-3">
                        <label for="deadline">Дата окончания заявки</label>
                        <input class="form-control {{ $errors->has('deadline') ? 'is-invalid' : '' }}"
                               value="{{ $task->deadline }}" type="date" name="deadline" id="deadline">
                        @if($errors->has('deadline'))
                            <div class="invalid-feedback">
                                {{ $errors->first('deadline') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="body">Описание заявки</label>
                    <textarea class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }} body" type="body"
                              name="body" id="body">{{ old('body', $task->raw_body) }}</textarea>
                    @if($errors->has('body'))
                        <div class="invalid-feedback">
                            {{ $errors->first('body') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="observers">Наблюдатели</label>
                        <div style="padding-bottom: 4px">
                            <span class="btn btn-info btn-xs select-all"
                                  style="border-radius: 0">{{ trans('global.select_all') }}</span>
                            <span class="btn btn-info btn-xs deselect-all"
                                  style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                        </div>
                        <select class="form-control select2 {{ $errors->has('observers') ? 'is-invalid' : '' }}"
                                name="observers[]" id="observers" multiple>
                            @foreach($users as $id => $user)
                                <option
                                    value="{{ $id }}" {{ in_array($id, old('observers', $task->observers->pluck('id', 'id')->toArray())) ? 'selected' : '' }}>{{ $user }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('observers'))
                            <div class="invalid-feedback">
                                {{ $errors->first('observers') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="responsible">Ответственные</label>
                        <div style="padding-bottom: 4px">
                            <span class="btn btn-info btn-xs select-all"
                                  style="border-radius: 0">{{ trans('global.select_all') }}</span>
                            <span class="btn btn-info btn-xs deselect-all"
                                  style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                        </div>
                        <select class="form-control select2 {{ $errors->has('responsible') ? 'is-invalid' : '' }}"
                                name="responsible[]" id="responsible" multiple>
                            @foreach($users as $id => $user)
                                <option
                                    value="{{ $id }}" {{ in_array($id, old('responsible', $task->responsible->pluck('id', 'id')->toArray())) ? 'selected' : '' }}>{{ $user }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('responsible'))
                            <div class="invalid-feedback">
                                {{ $errors->first('responsible') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-6">
                        <label for="responsible">Постановщик</label>

                        <select class="form-control {{ $errors->has('owner') ? 'is-invalid' : '' }}"
                                name="owner" id="owner">
                            @foreach($users as $id => $user)
                                <option
                                    value="{{ $id }}" {{ in_array($id, old('owner', $task->owner->pluck('id', 'id')->toArray())) ? 'selected' : '' }}>{{ $user }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('owner'))
                            <div class="invalid-feedback">
                                {{ $errors->first('owner') }}
                            </div>
                        @endif
                    </div>


                </div>
                <a href="{{ config('app.url').'/storage/'.$task->file }}" target="_blank">Текущий файл</a>
                <div class="form-group">
                    <label class="required" for="title">Выберите новый файл если хотите его заменить</label>
                    <input class="form-control {{ $errors->has('file') ? 'is-invalid' : '' }}" type="file" name="file"
                           id="file" value="{{ old('file', '') }}">
                    @if($errors->has('file'))
                        <div class="invalid-feedback">
                            {{ $errors->first('file') }}
                        </div>
                    @endif
                </div>



                @if(count($taskMedia) > 0)
                    <div class="card__file">
                        <h6>Дополнительные файлы:</h6>
                        @foreach($taskMedia as $media)
                            @if (Storage::disk('public')->has($media->path))
                                <a href="{{ config('app.url').'/storage/'.$media->path }}"
                                   target="_blank">{{ $media->original_name }}</a>
                                <small class="badge badge-danger card__file--delete" data-id="{{ $media->id }}">Удалить файл</small>
                                <br>
                            @endif
                        @endforeach
                    </div>
                @endif

                <div class="form-group">
                    <label class="required" for="title">Добавьте файл</label>
                    <div class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple/>
                        </div>
                    </div>

                </div>




                <div class="form-group">
                    <button class="btn btn-danger" id="save-task" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Комментарии
        </div>
        <div class="card-body">
            <div class=" offset-md-2 col-md-8 mb-3">
                @foreach($task->comments as $comment)
                    <div class="comment-wrapper mt-30">
                        <div class="card bg-light mb-3">
                            <div class="card-header"><span>{{ $comment->user->name }}</span>
                                <small>{{ $comment->created_at }}</small></div>
                            <div class="card-body">
                                <p class="card-text">
                                    {!! $comment->body !!}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
                <hr>
                <form action="{{ route('admin.tasks.comments.store', $task->id) }}" method="POST">
                    @csrf
                    <div class="wpapp mb-3">
                        <textarea name="body" id="body" class="form-control comment"></textarea>

                    </div>
                    <button class="btn btn-info" type="submit">Оставить комментарий</button>
                </form>

            </div>
        </div>
    </div>


@endsection


@section('scripts')
    <script src="{{ asset('js/pages/tasks.js') }}"></script>
    <script src="{{ asset('js/pages/tasks-show.js') }}"></script>
@endsection
