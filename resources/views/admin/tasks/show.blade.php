@extends('layouts.admin')
@section('content')
    <input hidden id="task-id" type="text" value="{{$task->id}}"/>

    <div class="container-fluid">
        <div class="row">
            <a class="btn btn-default" href="{{ route('admin.tasks.index') }}">
                Назад к списку
            </a>
            <div class="ml-auto">Постановщик: @foreach($task->owner as $key => $owner)
                    <span class="label label-info">{{ $owner->name }}</span>
                @endforeach</div>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-header">
            <span>
                Просмотр заявки
            </span>

        </div>

        <div class="card-body">
            <div class="form-group">
                <div class="form-group">

                </div>
                <div class="card__title">
                    <h3>{{ $task->title }}</h3>
                </div>
                <hr>
                <div class="card__body">
                    {!! $task->body !!}
                </div>

                @if($task->file)
                    <div class="card__file">
                        <h6>Основной файл:</h6>
                        @if (Storage::disk('public')->has($task->file))
                            <a href="{{ config('app.url').'/storage/'.$task->file }}"
                               target="_blank">{{ $task->original_file }}</a>
                        @endif
                    </div>
                    <br>
                @endif


                @if(count($taskMedia) > 0)
                    <div class="card__file">
                        <h6>Дополнительные файлы:</h6>
                        @foreach($taskMedia as $media)
                            @if (Storage::disk('public')->has($media->path))
                                <a href="{{ config('app.url').'/storage/'.$media->path }}"
                                   target="_blank">{{ $media->original_name }}</a>
                                <small class="badge badge-danger card__file--delete" data-id="{{ $media->id }}">Удалить файл</small>
                                <br>
                            @endif
                        @endforeach
                    </div>
                @endif

                <div class="form-group">
                    <label class="required" for="title">Добавьте файл</label>
                    <div class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple/>
                        </div>
                    </div>

                </div>

                @foreach($task->observers as $observer)
                    @if($observer->id == Auth::id() && !$task->is_approved)
                        <div class="card_approved">
                            <form action="{{ route('admin.tasks.approved', $task->id) }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <button class="btn btn-success width-250" type="submit">Согласовать</button>
                            </form>
                        </div>
                    @endif
                @endforeach
                <hr>
                @foreach($task->comments as $comment)
                    <div class="comment-wrapper mt-30">
                        <div class="card bg-light mb-3">
                            <div class="card-header"><span>{{ $comment->user->name }}</span>
                                <small>{{ $comment->created_at }}</small></div>
                            <div class="card-body">
                                <p class="card-text">
                                    {!! $comment->body !!}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach

                <form action="{{ route('admin.tasks.comments.store', $task->id) }}" method="POST">
                    @csrf
                    <div class="wpapp mb-3">

                        <label for="exampleFormControlTextarea1">Комментарий</label>
                        <textarea class="form-control comment" name="body" id="body" rows="3"></textarea>

                    </div>
                    <button class="btn btn-info width-250" type="submit">Оставить комментарий</button>
                </form>
                <br>
                <div class="card__responsible">
                    Ответственный:
                    @foreach($task->responsible as $key => $responsible)
                        <span class="label label-info">{{ $responsible->name }}</span>
                    @endforeach
                </div>
                <div class="card__deadline">
                    Дата окончания: {{ $task->deadline }}
                </div>

                <div class="card__delete">
                    @can('task_delete')
                        <form action="{{ route('admin.tasks.destroy', $task->id) }}" method="POST"
                              onsubmit="return confirm('{{ trans('global.areYouSure') }}');"
                              style="display: inline-block;">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-xs btn-danger" value="Закрыть заявку">
                        </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/pages/tasks-show.js') }}"></script>
@endsection
