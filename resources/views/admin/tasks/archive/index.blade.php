@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            Список архивных задач
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                    <thead>
                    <tr>
                        <th></th>

                        <th>Плательщик</th>
                        <th>Контрагент</th>
                        <th>Сумма</th>
                        <th>Назначение</th>
                        <th>Постановщик</th>
                        <th>Ответственные</th>
                        <th> Дата окончания</th>
                        <th>Дата создания</th>
                        <th>Дата Закрытия</th>
                        <th>Согласовано</th>

                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $key => $task)
                        <tr data-entry-id="{{ $task->id }}">
                            <td></td>

                            <td>
                                {{ $task->project ?? '' }}
                            </td>

                            <td>
                                {{ $task->contragent ?? '' }}
                            </td>
                            <td>
                                {{ $task->price ?? '' }}
                            </td>
                            <td>
                                {{ $task->appointment ?? '' }}
                            </td>


                            <td>
                                @foreach($task->owner as $owner)
                                    <span class="badge badge-info">{{ $owner->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach($task->responsible as $responsible)
                                    <span class="badge badge-info">{{ $responsible->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                {{ $task->deadline }}
                            </td>
                            <td>
                                {{ $task->created_at }}
                            </td>
                            <td>
                                {{ $task->deleted_at }}
                            </td>
                            <td>
                                @foreach($task->observers as $observer)
                                    @if($observer->pivot->is_approved)
                                        <span class="badge badge-success">{{ $observer->name }}</span>
                                    @else
                                        <span class="badge badge-danger">{{ $observer->name }}</span>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @can('task_restore')
                                    <a class="btn btn-xs btn-info"
                                       href="{{ route('admin.archive.tasks.restore', $task->id) }}">
                                        Восстановить
                                    </a>
                                @endcan

                                @can('task_show')
                                    <a class="btn btn-xs btn-primary"
                                       href="{{ route('admin.archive.tasks.show', $task->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('task_force_delete')
                                    <form action="{{ route('admin.archive.tasks.destroy', $task->id) }}" method="POST"
                                          onsubmit="return confirm('Вы уверены?');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger"
                                               value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('task_delete')
            {{--let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'--}}
            {{--let deleteButton = {--}}
            {{--    text: deleteButtonTrans,--}}
            {{--    url: "{{ route('admin.archive.tasks.massDestroy') }}",--}}
            {{--    className: 'btn-danger',--}}
            {{--    action: function (e, dt, node, config) {--}}
            {{--        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {--}}
            {{--            return $(entry).data('entry-id')--}}
            {{--        });--}}

            {{--        if (ids.length === 0) {--}}
            {{--            alert('{{ trans('global.datatables.zero_selected') }}')--}}

            {{--            return--}}
            {{--        }--}}

            {{--        if (confirm('{{ trans('global.areYouSure') }}')) {--}}
            {{--            $.ajax({--}}
            {{--                headers: {'x-csrf-token': _token},--}}
            {{--                method: 'POST',--}}
            {{--                url: config.url,--}}
            {{--                data: { ids: ids, _method: 'DELETE' }})--}}
            {{--                .done(function () { location.reload() })--}}
            {{--        }--}}
            {{--    }--}}
            {{--}--}}
            {{--dtButtons.push(deleteButton)--}}
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                orderCellsTop: true,
                order: [[1, 'desc']],
                pageLength: 100,
            });
            let table = $('.datatable-User:not(.ajaxTable)').DataTable({buttons: dtButtons})
            $('a[data-toggle="tab"]').on('shown.bs.tab click', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });

        })

    </script>
@endsection
