<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
{{--        <li class="c-sidebar-nav-item">--}}
{{--            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">--}}
{{--                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">--}}

{{--                </i>--}}
{{--                {{ trans('global.dashboard') }}--}}
{{--            </a>--}}
{{--        </li>--}}
        @can('task_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/tasks") || request()->is("admin/tasks/*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa fa-tasks c-sidebar-nav-icon">

                    </i>
                    Заявки
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('task_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.tasks.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/tasks") || request()->is("admin/tasks/*") ? "active" : "" }}">
                                <i class="fa fa-thumb-tack c-sidebar-nav-icon">

                                </i>
                               Активные заявки
                            </a>
                        </li>
                    @endcan
                    @can('task_marketica')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.tasks.marketica") }}" class="c-sidebar-nav-link {{ request()->is("admin/tasks") || request()->is("admin/tasks/*") ? "active" : "" }}">
                                <i class="fa fa-thumb-tack c-sidebar-nav-icon">

                                </i>
                               Задачи Маркетики
                            </a>
                        </li>
                    @endcan
                    @can('task_archive_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.archive.tasks.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/archive/tasks") || request()->is("admin/archive/tasks/*") ? "active" : "" }}">
                                <i class="fa fa-archive c-sidebar-nav-icon">

                                </i>
                                Архив заявок
                            </a>
                        </li>
                    @endcan
                    @can('task_create')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.tasks.create") }}" class="c-sidebar-nav-link {{ request()->is("admin/tasks/create") || request()->is("admin/tasks/create/*") ? "active" : "" }}">
                                <i class="fa fa-plus c-sidebar-nav-icon">

                                </i>
                                Создать заявку
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                Разрешения
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                Роли
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                Пользователи
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('project_management_access')
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link {{ request()->is('admin/projects') || request()->is('admin/projects/*') ? 'active' : '' }}" href="{{ route('admin.projects.index') }}">
                    <i class="fa fa-address-card c-sidebar-nav-icon">
                    </i>
                    Плательщики
                </a>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        Сменить пароль
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>
