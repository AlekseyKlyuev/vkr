<?php

// Главная
Breadcrumbs::for('admin.home', function ($trail) {
    $trail->push('Главная', route('admin.home'));
});

// Главная > Задачи
Breadcrumbs::for('admin.tasks.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Заявки', route('admin.tasks.index'));
});

// Главная > Задачи >  Создать
Breadcrumbs::for('admin.tasks.create', function ($trail) {
    $trail->parent('admin.tasks.index');
    $trail->push('Создать', route('admin.tasks.create'));
});

// Главная > Задачи > Редактировать
Breadcrumbs::for('admin.tasks.edit', function ($trail, $task) {
    $trail->parent('admin.tasks.index');
    $trail->push('Редактировать ', route('admin.tasks.edit', $task->id));
});

// Главная > Задачи > Посмотреть
Breadcrumbs::for('admin.tasks.show', function ($trail, $task) {
    $trail->parent('admin.tasks.index');
    $trail->push('Просмотр ', route('admin.tasks.show', $task));
});

// Главная > Задачи > Редактировать
Breadcrumbs::for('admin.archive.tasks.index', function ($trail) {
    $trail->parent('admin.tasks.index');
    $trail->push('Архив', route('admin.archive.tasks.index'));
});

// Главная > Задачи > Маркетика
Breadcrumbs::for('admin.tasks.marketica', function ($trail) {
    $trail->push('Задачи Маркетики', route('admin.tasks.marketica'));
});

// Главная > Пользователь
Breadcrumbs::for('admin.users.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Пользователи', route('admin.users.index'));
});

// Главная > Пользователь >  Создать
Breadcrumbs::for('admin.users.create', function ($trail) {
    $trail->parent('admin.users.index');
    $trail->push('Создать', route('admin.users.create'));
});

// Главная > Пользователь > Редактировать
Breadcrumbs::for('admin.users.edit', function ($trail, $user) {
    $trail->parent('admin.users.index');
    $trail->push('Редактировать ', route('admin.users.edit', $user->id));
});

// Главная > Пользователь > Показать
Breadcrumbs::for('admin.users.show', function ($trail, $user) {
    $trail->parent('admin.users.index');
    $trail->push('Показать ', route('admin.users.show', $user->id));
});

// Главная > Разрешения
Breadcrumbs::for('admin.permissions.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Разрешения', route('admin.permissions.index'));
});

// Главная > Разрешения > Создать
Breadcrumbs::for('admin.permissions.create', function ($trail) {
    $trail->parent('admin.permissions.index');
    $trail->push('Создать', route('admin.permissions.create'));
});

// Главная > Разрешения > Редактировать
Breadcrumbs::for('admin.permissions.update', function ($trail) {
    $trail->parent('admin.permissions.index');
    $trail->push('Редактировать', route('admin.permissions.update'));
});

// Главная > Разрешения > Редактировать
Breadcrumbs::for('admin.permissions.edit', function ($trail, $permission) {
    $trail->parent('admin.permissions.index');
    $trail->push('Редактировать', route('admin.permissions.edit', $permission->id));
});

// Главная > Разрешения > Редактировать
Breadcrumbs::for('admin.permissions.show', function ($trail, $permission) {
    $trail->parent('admin.permissions.index');
    $trail->push('Редактировать', route('admin.permissions.show', $permission->id));
});

// Главная > Роли
Breadcrumbs::for('admin.roles.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Роли', route('admin.roles.index'));
});

// Главная > Роли > Создать
Breadcrumbs::for('admin.roles.create', function ($trail) {
    $trail->parent('admin.roles.index');
    $trail->push('Создать', route('admin.roles.create'));
});

// Главная > Роли > Редактировать
Breadcrumbs::for('admin.roles.show', function ($trail, $role) {
    $trail->parent('admin.roles.index');
    $trail->push('Показать ', route('admin.roles.show', $role->id));
});

// Главная > Роли > Редактировать
Breadcrumbs::for('admin.roles.edit', function ($trail, $role) {
    $trail->parent('admin.roles.index');
    $trail->push('Редактировать ', route('admin.roles.edit', $role->id));
});

// Главная > Разрешения > Редактировать
Breadcrumbs::for('profile.password.edit', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Смена пароля', route('profile.password.edit'));
});

// Главная > Разрешения > Редактировать
Breadcrumbs::for('admin.tasks.approved', function ($trail, $task) {
    $trail->parent('admin.home');
    $trail->push('Согласование', route('admin.tasks.approved', $task->id));
});

// Главная > Проекты
Breadcrumbs::for('admin.projects.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Плательщики', route('admin.projects.index'));
});

// Главная > Проекты >  Создать
Breadcrumbs::for('admin.projects.create', function ($trail) {
    $trail->parent('admin.projects.index');
    $trail->push('Создать', route('admin.projects.create'));
});

// Главная > Проекты > Редактировать
Breadcrumbs::for('admin.projects.edit', function ($trail, $project) {
    $trail->parent('admin.projects.index');
    $trail->push('Редактировать ', route('admin.projects.edit', $project->id));
});

// Главная > Проекты > Показать
Breadcrumbs::for('admin.projects.show', function ($trail, $project) {
    $trail->parent('admin.projects.index');
    $trail->push('Показать ', route('admin.projects.show', $project->id));
});


// Главная > Ахивные задачи
Breadcrumbs::for('admin.archive.tasks.show', function ($trail) {
    $trail->push('Архивная задача', route('admin.archive.tasks.index'));
});

//// Главная > Разрешения > Редактировать
//Breadcrumbs::for('category', function ($trail, $category) {
//    $trail->parent('blog');
//    $trail->push($category->title, route('category', $category->id));
//});

//// Home > Blog > [Category] > [Post]
//Breadcrumbs::for('post', function ($trail, $post) {
//    $trail->parent('category', $post->category);
//    $trail->push($post->title, route('post', $post->id));
//});
// Главная > Разрешения > Редактировать
//Breadcrumbs::for('category', function ($trail, $category) {
//    $trail->parent('blog');
//    $trail->push($category->title, route('category', $category->id));
//});
