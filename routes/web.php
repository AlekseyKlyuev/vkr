<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Route::get('/home', function () {
//    if (session('status')) {
//        return redirect()->route('admin.home')->with('status', session('status'));
//    }
//
//    return redirect()->route('admin.home');
//});

Route::get('/', function () {
    return redirect()->route('admin.tasks.index');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('home');

    Route::resource('permissions', 'PermissionsController');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    Route::delete('tasks/destroy', 'TasksController@massDestroy')->name('tasks.massDestroy');
    Route::patch('tasks/approved/{task}', 'TasksController@approved')->name('tasks.approved');
    Route::get('/tasks/marketica', 'TasksController@observeMarketica')->name('tasks.marketica');
    Route::resource('tasks', 'TasksController');

    Route::get('archive/tasks', 'ArchiveTaskController@index')->name('archive.tasks.index');

    Route::get('archive/tasks/{task}', 'ArchiveTaskController@restore')->name('archive.tasks.restore');
    Route::get('archive/deleted-tasks/{id?}', 'ArchiveTaskController@show')->name('archive.tasks.show');
    Route::delete('archive/tasks/{task}', 'ArchiveTaskController@destroy')->name('archive.tasks.destroy');
    Route::get('archive/tasks/destroy', 'ArchiveTaskController@massDestroy')->name('archive.tasks.massDestroy');

    Route::resource('projects', 'ProjectsController');

    Route::resource('tasks.comments', 'TaskCommentsController')->only('store')->shallow();
});

/*
 * сделать сменить вкладку сменить пароль для пользователей
 * */
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});

Route::post('telegram/webhook', 'Telegram\TelegramController@webhookHandler')->name('webhook');
Route::get('telegram/setWebhook', 'Telegram\TelegramController@setWebhook')->name('webhook');


Route::get('telegram/push-message', 'Telegram\TelegramPushMessageController@pushMessageForAllUsers');

//Route::get('test', 'TestController@test');

// files
Route::post('dropzone/add/{id}', 'Admin\MediaTaskController@store');
Route::post("media-task/destroy", 'Admin\MediaTaskController@destroy');

Route::middleware('auth')->get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
